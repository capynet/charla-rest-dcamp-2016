<?php

/**
 * @file
 * Contains vote_entity.page.inc.
 *
 * Page callback for Vote entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Vote entity templates.
 *
 * Default template: vote_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_vote_entity(array &$variables) {
  // Fetch VoteEntity Entity Object.
  $vote_entity = $variables['elements']['#vote_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
