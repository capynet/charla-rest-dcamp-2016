<?php

/**
 * @file
 * Contains \Drupal\vote\VoteEntityInterface.
 */

namespace Drupal\vote;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Vote entity entities.
 *
 * @ingroup vote
 */
interface VoteEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.
  /**
   * Gets the Vote entity name.
   *
   * @return string
   *   Name of the Vote entity.
   */
  public function getName();

  /**
   * Sets the Vote entity name.
   *
   * @param string $name
   *   The Vote entity name.
   *
   * @return \Drupal\vote\VoteEntityInterface
   *   The called Vote entity entity.
   */
  public function setName($name);

  /**
   * Gets the Vote entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Vote entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Vote entity creation timestamp.
   *
   * @param int $timestamp
   *   The Vote entity creation timestamp.
   *
   * @return \Drupal\vote\VoteEntityInterface
   *   The called Vote entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Vote entity published status indicator.
   *
   * Unpublished Vote entity are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Vote entity is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Vote entity.
   *
   * @param bool $published
   *   TRUE to set this Vote entity to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\vote\VoteEntityInterface
   *   The called Vote entity entity.
   */
  public function setPublished($published);

}
