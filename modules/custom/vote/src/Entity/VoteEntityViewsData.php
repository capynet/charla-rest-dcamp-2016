<?php

/**
 * @file
 * Contains \Drupal\vote\Entity\VoteEntity.
 */

namespace Drupal\vote\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Vote entity entities.
 */
class VoteEntityViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['vote_entity']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Vote entity'),
      'help' => $this->t('The Vote entity ID.'),
    );

    return $data;
  }

}
