<?php

/**
 * @file
 * Contains \Drupal\vote\VoteEntityAccessControlHandler.
 */

namespace Drupal\vote;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Vote entity entity.
 *
 * @see \Drupal\vote\Entity\VoteEntity.
 */
class VoteEntityAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\vote\VoteEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished vote entity entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published vote entity entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit vote entity entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete vote entity entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add vote entity entities');
  }

}
