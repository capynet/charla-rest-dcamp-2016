<?php

/**
 * @file
 * Contains \Drupal\vote\Form\VoteEntityForm.
 */

namespace Drupal\vote\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Vote entity edit forms.
 *
 * @ingroup vote
 */
class VoteEntityForm extends ContentEntityForm {
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\vote\Entity\VoteEntity */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Vote entity.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Vote entity.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.vote_entity.canonical', ['vote_entity' => $entity->id()]);
  }

}
