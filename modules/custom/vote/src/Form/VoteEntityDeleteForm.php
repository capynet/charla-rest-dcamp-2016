<?php

/**
 * @file
 * Contains \Drupal\vote\Form\VoteEntityDeleteForm.
 */

namespace Drupal\vote\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Vote entity entities.
 *
 * @ingroup vote
 */
class VoteEntityDeleteForm extends ContentEntityDeleteForm {

}
