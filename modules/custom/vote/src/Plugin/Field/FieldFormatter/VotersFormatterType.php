<?php

/**
 * @file
 * Contains \Drupal\vote\Plugin\Field\FieldFormatter\VotersFormatterType.
 */

namespace Drupal\vote\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

/**
 * Plugin implementation of the 'voters_formatter_type' formatter.
 *
 * @FieldFormatter(
 *   id = "voters_formatter_type",
 *   label = @Translation("Voters formatter"),
 *   field_types = {
 *     "voters_field_type"
 *   }
 * )
 */
class VotersFormatterType extends FormatterBase {
  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(// Implement default settings.
    ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return array(// Implement settings form.
    ) + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $divisor = 0;
    $puntaje = 0;

    $node = \Drupal::routeMatch()->getParameter('node');
    $query = \Drupal::entityQuery('vote_entity');
    $query->condition('status', 1);
    $query->condition('field_session.target_id', $node->id());
    $votos_nid = $query->execute();
    $votos = \Drupal::entityTypeManager()->getStorage('vote_entity')->loadMultiple($votos_nid);

    foreach ($votos as $voto) {
      $puntaje += (int)$voto->get('field_pun')->getValue()[0]['value'];
      $divisor++;
    }

    $elements[] = ['#markup' => $divisor];

    return $elements;
  }

}
