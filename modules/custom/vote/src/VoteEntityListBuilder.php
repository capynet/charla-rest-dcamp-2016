<?php

/**
 * @file
 * Contains \Drupal\vote\VoteEntityListBuilder.
 */

namespace Drupal\vote;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Vote entity entities.
 *
 * @ingroup vote
 */
class VoteEntityListBuilder extends EntityListBuilder {
  use LinkGeneratorTrait;
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Vote entity ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\vote\Entity\VoteEntity */
    $row['id'] = $entity->id();
    $row['name'] = $this->l(
      $entity->label(),
      new Url(
        'entity.vote_entity.edit_form', array(
          'vote_entity' => $entity->id(),
        )
      )
    );
    return $row + parent::buildRow($entity);
  }

}
