<?php

namespace Drupal\user_for_app\Plugin\rest\resource;

use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Psr\Log\LoggerInterface;


/**
 * Provides a resource to auto create users by token.
 *
 * @RestResource(
 *   id = "user_for_app",
 *   label = @Translation("Users for app"),
 *   uri_paths = {
 *     "canonical" = "/user-for-app/{token}"
 *   },
 * )
 */
class UserResource extends ResourceBase {
  /**
   *  A curent user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;
  /**
   *  A instance of entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('entity.manager'),
      $container->get('current_user')
    );
  }

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    EntityManagerInterface $entity_manager,
    AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->entityManager = $entity_manager;
    $this->currentUser = $current_user;
  }

  /**
   * Responds to GET requests.
   *
   * Returns a new user for an app.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The response containing a list of bundle names.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   */
  public function get($token = NULL) {
//    if ($token) {
//      $user_for_app = [
//        'token_app' => $token,
//        'uid' => 1000,
//        'name' => 'Señor foo'
//      ];
//
//      return new ResourceResponse($user_for_app);
//    }
//
//    throw new HttpException(t('Token wasn\'t provided'));
  }

  /**
   * Responds to GET requests.
   *
   * Returns a new user for an app.
   *
   * @param array $data Incomming POST data.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The response containing a list of bundle names.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   */
  public function post($data = array()) {

    $rand = rand(0, 99999999999);
    $fake_username = 'user_for_app' . $rand;

    $user = User::create();
    $user->setPassword($rand);
    $user->enforceIsNew();
    $user->setEmail($fake_username . '@internal.fake');
    $user->setUsername($fake_username);
    $user->set('field_token_app', $data['deviceId']);
    $user->addRole('user_for_app');
    $user->activate();
    $user->save();

    /** @var \Drupal\Core\Entity\Sql\SqlContentEntityStorage $at */
    $at = \Drupal::service('entity_type.manager')->getStorage('access_token');

    // 31536000 -> 1 año
    $values = [
      'expire' => REQUEST_TIME + 31536000,
      'user_id' => 1,
      'auth_user_id' => $user->get('uid')->value,
      'resource' => 'global',
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
    ];

    /** @var \Drupal\simple_oauth\Entity\AccessToken $g */
    $g = $at->create($values);
    $g->save();

    // Entreguemos el token de oauth para que pueda comenzar a operar con este a partir de ahora.
    $oauth_token = reset($g->get('value')->getValue());

    return new ResourceResponse([
      'oauth_token' => $oauth_token['value'],
      'uid' => $user->get('uid')->value
    ]);

  }
}
