<?php

/**
 * @file
 * Contains \Drupal\onesignal\OneSignal.
 */

namespace Drupal\onesignal;


/**
 * Class OneSignal.
 *
 * @package Drupal\onesignal
 */
class OneSignal {
  /**
   * Constructor.
   */
  public function __construct() {

  }

  public function pushNotification($text) {
    $content = array(
      "en" => $text
    );

    $fields = array(
      'app_id' => "8dc3982b-a222-405f-8b26-cb877ed0978f",
      'included_segments' => array('All'),
      'contents' => $content
    );

    $fields = json_encode($fields);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Content-Type: application/json',
      'Authorization: Basic MGMwY2ZlMjItYzQ1OC00YWQxLTgzNDItY2RhMGI3NmM2ZmMx'
    ));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

    $response = curl_exec($ch);
    curl_close($ch);

    return $response;
  }

}
