<?php

/**
 * @file
 * Contains \Drupal\rest_test\Plugin\rest\resource\SerializationClassTestResource.
 */

namespace Drupal\rest_test\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;

/**
 * Class used to test that a REST resource don't need a serialization class.
 *
 * @RestResource(
 *   id = "serialization_test",
 *   label = @Translation("Serialization Class Not Needed"),
 *   serialization_class = ""
 * )
 */
class SerializationClassTestResource extends ResourceBase {

  /**
   * Responds to serialization test POST request.
   *
   * @param array $array
   *   An array with test information.
   *
   *
   * @return \Drupal\rest\ResourceResponse
   */
  public function post(array $array = []) {
    return new ResourceResponse($array[0], 200, []);
  }

}